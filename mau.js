var my_instances = [];



function create_elements(value){
	var div1 = document.createElement('div'), div2 = document.createElement('div'),
		deleteButton = document.createElement('i'), editButton = document.createElement('i');
	div1.setAttribute('class', 'container');
	div2.setAttribute('class', 'text-container');
	deleteButton.setAttribute('class', 'buttons remove');
	editButton.setAttribute('class', 'buttons edit');

	div2.textContent = value;

	apend_childs(editButton, deleteButton, div2, div1);
}
function apend_childs(editButton, deleteButton, div2, div1){
	div1.appendChild(editButton);
	div1.appendChild(deleteButton);
	div1.appendChild(div2);
	document.getElementById('big-container').appendChild(div1);
	my_instances.push(div1);
}

var recoverFormValue = document.getElementById('myform');
recoverFormValue.addEventListener('submit', function(e) {
	create_elements(e.target.firstElementChild.value);
	e.preventDefault();
});
function edit_function(value){
	let confirme = confirm("Voulez vous vraiment modifier ce text??");
	if(confirme){
		value = prompt('Veillez sélectionner votre texte ci-dessous si vous voulez le modifier et non le remplacer\n\n\t' + value);
		return value;
	}
	else {
		return value;
	}

}

function delete_function(target){
	let confirme = confirm("Voulez-vous réellement supprimer ce champ??");
	if(confirme)
	{
		target.parentNode.removeChild(target);
	}
}
var listener = document.getElementById('big-container');
listener.addEventListener('click', function(e){
	let target = e.target;
	if(target.className == 'buttons edit')
	{
		let updateText = target.parentNode.querySelector('.text-container');
		updateText.textContent = edit_function(target.parentNode.textContent);
	}
	else if(target.className == 'buttons remove')
	{
		let deleteElement = target.parentNode;
		delete_function(deleteElement);

	}
});